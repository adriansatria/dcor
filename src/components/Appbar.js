// TUGAS UAS
// NAMA : ADRIAN SATRIA PUTRA
// NIM  : 10123902
// KELAS: IF12

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import Image from "next/image";
import {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {CssBaseline, Fade, useScrollTrigger} from "@mui/material";
import PropTypes from 'prop-types';
import Fab from '@mui/material/Fab';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

const pages = [
    {
        page: 'Beranda', path: '/'
    },
    {
        page: 'Tentang Kami', path: '/#tentang-kami'
    },
    {
        page: 'Pelayanan', path: '/#layanan-kami'
    },
    {
        page: 'Portofolio', path: '/#portofolio'
    },
    {
        page: 'Kontak', path: '/#contact'
    },
]

const ScrollTop = (props) => {
    const {children, window} = props;
    // Note that you normally won't need to set the window ref as useScrollTrigger
    // will default to window.
    // This is only being set here because the demo is in an iframe.
    const trigger = useScrollTrigger({
        target: window ? window() : undefined,
        disableHysteresis: true,
        threshold: 100,
    });

    const handleClick = (event) => {
        const anchor = (event.target.ownerDocument || document).querySelector(
            '#back-to-top-anchor',
        );

        if (anchor) {
            anchor.scrollIntoView({
                block: 'center',
            });
        }
    };

    return (
        <Fade in={trigger}>
            <Box
                onClick={handleClick}
                role="presentation"
                sx={{position: 'fixed', bottom: 16, right: 16}}
            >
                {children}
            </Box>
        </Fade>
    );
}

ScrollTop.propTypes = {
    children: PropTypes.element.isRequired,
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

const ResponsiveAppBar = (props) => {
    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);
    const [path, setPath] = useState('');

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const router = useRouter();

    const handleCloseNavMenu = async (path) => {
        setAnchorElNav(null);

        if (path) {
            await router.push(path)
        }
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <>
            <CssBaseline />
            <AppBar position="static" sx={{backgroundColor: 'white'}} component={'nav'} id="back-to-top-anchor">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <AdbIcon sx={{display: {xs: 'none', md: 'flex'}, mr: 1}}/>
                        <Typography
                            variant="h6"
                            noWrap
                            component="a"
                            href="#"
                            sx={{
                                mr: 2,
                                display: {xs: 'none', md: 'flex'},
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                            <Image src={`${process.env.NEXT_PUBLIC_BASE_URL}/dcor-logo.svg`} width={90} height={90}
                                   alt={"Logo DCOR."}/>
                        </Typography>

                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href="#"
                            sx={{
                                mr: 2,
                                display: {xs: 'flex', md: 'none'},
                                flexGrow: 1,
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                            <Image src={`${process.env.NEXT_PUBLIC_BASE_URL}/dcor-logo.svg`} width={90} height={90}
                                   alt={"Logo DCOR."}/>
                        </Typography>
                        <Box sx={{display: {xs: 'none', md: 'flex'}, marginLeft: 'auto'}}>
                            {pages.map((page, index) => (
                                <Button
                                    key={index}
                                    onClick={() => {
                                        handleCloseNavMenu(page.path)
                                    }}
                                    sx={{my: 2, color: 'black', display: 'block'}}
                                >
                                    {page.page}
                                </Button>
                            ))}
                        </Box>
                        <Box sx={{marginLeft: 5}}>
                            <Button sx={{backgroundColor: 'black'}} variant={'tonal'} size={'small'}>Login</Button>
                        </Box>

                        <Box sx={{display: {xs: 'flex', md: 'none'}}}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleOpenNavMenu}
                                color="inherit"
                            >
                                <MenuIcon sx={{color: 'black'}}/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorElNav}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                open={Boolean(anchorElNav)}
                                onClose={handleCloseNavMenu}
                                sx={{
                                    display: {xs: 'block', md: 'none'},
                                }}
                            >
                                {pages.map((page, index) => (
                                    <MenuItem key={index} onClick={() => {
                                        handleCloseNavMenu(page.path)
                                    }}>
                                        <Typography textAlign="center">{page.page}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
            <ScrollTop {...props}>
                <Fab size="small" aria-label="scroll back to top">
                    <KeyboardArrowUpIcon />
                </Fab>
            </ScrollTop>
        </>
    );
}

export default ResponsiveAppBar;