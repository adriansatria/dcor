// TUGAS UAS
// NAMA : ADRIAN SATRIA PUTRA
// NIM  : 10123902
// KELAS: IF12

import styles from "../styles/home.module.css";
import {Divider, Grid, Link, Stack} from "@mui/material";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EmailIcon from "@mui/icons-material/Email";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";

const ResponsiveFooter = () => {
    const contactInformation = [
        {
            icon: <LocationOnIcon fontSize={'large'}/>,
            text: 'Jl. Ciganitri Tengah No. 20 RT 004/002'
        },
        {
            icon: <EmailIcon fontSize={'large'}/>,
            text: 'dcor.company@gmail.com'
        },
        {
            icon: <WhatsAppIcon fontSize={'large'}/>,
            text: '+851-5912-23456'
        }
    ];

    const socialMedia = [
        {
            image: `${process.env.NEXT_PUBLIC_BASE_URL}/facebook.png`,
            label: 'facebook',
            link: '#',
        },
        {
            image: `${process.env.NEXT_PUBLIC_BASE_URL}/instagram.png`,
            label: 'instagram',
            link: '#',
        },
        {
            image: `${process.env.NEXT_PUBLIC_BASE_URL}/twitter.png`,
            label: 'twitter',
            link: '#',
        },
        {
            image: `${process.env.NEXT_PUBLIC_BASE_URL}/youtube.png`,
            label: 'youtube',
            link: '#',
        },
    ]

    return (
        <Box className={styles.customMarginSection}>
            <Divider textAlign="left">
                <Typography
                    variant={'h4'}
                    sx={{
                        fontWeight: 600,
                        marginY: 5
                    }}>
                    HUBUNGI KAMI
                </Typography>
            </Divider>
            <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={6}>

                    <Stack spacing={2} sx={{marginY: 5}}>
                        {contactInformation.map((contact, index) => (
                            <Box key={index}>
                                <Grid container spacing={0}>
                                    <Grid item xs={2} md={1}>
                                        {contact.icon}
                                    </Grid>
                                    <Grid item xs={9} md={10}>
                                        {contact.text}
                                    </Grid>
                                </Grid>
                            </Box>
                        ))}
                    </Stack>
                </Grid>
                <Grid item xs={12} sm={12} md={6}>
                    <Stack spacing={2} direction={'row'} justifyContent={{xs: 'center', md: 'flex-end'}}>
                        {socialMedia.map((sosmed, index) => (
                            <Link
                                key={index}
                                href={sosmed.link}
                                variant='body2'
                                target='_blank'
                            >
                                <img className={styles.sosmedIcon} src={sosmed.image} width="40px" height="40px"
                                     alt={sosmed.label}/>
                            </Link>
                        ))}
                    </Stack>
                    <Box textAlign={{xs: 'center', md: 'end'}}>
                        <img src={`${process.env.NEXT_PUBLIC_BASE_URL}/dcor-logo.svg`} width={200} height={200}
                             alt={'dcor-logo'}/>
                    </Box>
                </Grid>
            </Grid>
            <Typography sx={{
                marginY: 2,
                textAlign: 'center'
            }}>
                © 2011 DCOR. All Right Reserved
            </Typography>
        </Box>
    );
}

export default ResponsiveFooter