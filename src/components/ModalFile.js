import Box from "@mui/material/Box";
import {Dialog, DialogContent, Modal, styled} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';

const ModalFile = ({openModal, setOpenModal, src}) => {
    const CustomCloseButton = styled(IconButton)(({ theme }) => ({
        top: 0,
        right: 0,
        color: 'grey.500',
        position: 'absolute',
        boxShadow: theme.shadows[2],
        transform: 'translate(10px, -10px)',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: `${theme.palette.background.paper} !important`,
        transition: 'transform 0.25s ease-in-out, b`ox-shadow 0.25s ease-in-out',
        '&:hover': {
            transform: 'translate(7px, -5px)'
        }
    }))

    const handleClose = () => {
        setOpenModal(false)
    }

    return (
        <Dialog
            open={openModal}
            onClose={()=> {
                setOpenModal(false)
            }}
            fullWidth
            maxWidth='lg'
            scroll='body'
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            sx={{ '& .MuiDialog-paper': { overflow: 'visible' } }}
        >
            <DialogContent>
                <CustomCloseButton onClick={handleClose}>
                    <CloseIcon />
                </CustomCloseButton>

                <img src={src} width={'100%'} />
            </DialogContent>
        </Dialog>
    )
}

export default ModalFile