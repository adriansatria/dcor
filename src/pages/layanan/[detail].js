// TUGAS UAS
// NAMA : ADRIAN SATRIA PUTRA
// NIM  : 10123902
// KELAS: IF12

import {useRouter} from "next/router";
import Box from "@mui/material/Box";
import styles from '@/styles/home.module.css';
import Typography from "@mui/material/Typography";
import {Avatar, Divider, Grid, ImageListItemBar, Link, Stack} from "@mui/material";
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";
import Head from "next/head";
import Button from "@mui/material/Button";
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import ModalFile from "@/components/ModalFile";
import {useState} from "react";

const DetailPage = () => {
    const [openModal, setOpenModal] = useState(false)
    const [image, setImage] = useState('')

    const router = useRouter()
    const {detail} = router.query

    const srcImages = [
        {
            src: 'https://cdn.pixabay.com/photo/2016/12/30/07/59/kitchen-1940174_1280.jpg',
            text: 'Desain Interior',
            detail: 'design-interior'
        },
        {
            src: 'https://cdn.pixabay.com/photo/2016/11/18/17/46/house-1836070_1280.jpg',
            text: 'Arsitektur',
            detail: 'arsitektur'
        },
    ];

    let itemData = [];

    if (detail == 'design-interior') {
        itemData = [
            {
                img: 'https://cdn.pixabay.com/photo/2018/10/28/12/37/bedroom-3778695_1280.jpg',
                title: 'Bed Room',
                type: 'Bedroom'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2016/11/22/19/11/brick-wall-1850095_960_720.jpg',
                title: 'Living Room',
                type: 'Living Room'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2016/01/31/14/32/architecture-1171462_1280.jpg',
                title: 'Kitchen',
                type: 'Kitchen'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2018/02/13/09/39/modern-minimalist-bathroom-3150293_1280.jpg',
                title: 'Bathroom',
                type: 'Bathroom'
            },
        ];
    } else if (detail == 'arsitektur') {
        itemData = [
            {
                img: 'https://cdn.pixabay.com/photo/2015/11/06/11/48/multi-family-home-1026481_1280.jpg',
                title: 'Perumahan',
                type: 'Perumahan'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2015/11/06/11/39/single-family-home-1026369_1280.jpg',
                title: 'Villa',
                type: 'Villa'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2015/11/06/11/48/multi-family-home-1026484_960_720.jpg',
                title: 'Apartemen',
                type: 'Apartemen'
            },
            {
                img: 'https://cdn.pixabay.com/photo/2015/11/06/11/39/single-family-home-1026368_1280.jpg',
                title: 'Rumah Tunggal',
                type: 'Rumah Tunggal'
            },
        ];
    }


    const srcImagesDcor = [
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-one.svg`,
            text: 'Konsultasi Awal'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-two.svg`,
            text: 'Survey Lokasi'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-three.svg`,
            text: 'Proses Desain'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-four.svg`,
            text: 'Perencanaan RAB'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-five.svg`,
            text: 'Implementasi'
        },
    ];

    return (
        <>
            <Head>
                <title>DCOR. | Layanan Kami</title>
                <meta name="description"
                      content="Melayani pemesanan pembuatan furniture dan sewa jawa dekorasi rumah anda. Jadikan rumah anda menjadi impian bagi semua orang."/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href={`${process.env.NEXT_PUBLIC_BASE_URL}/dcor-rel-logo.svg`}/>
                <meta name='keywords' content='DCOR, Dekorasi rumah, dekorasi, Rumah dekorasi'/>
            </Head>
            <main>
                <section>
                    <Box className={styles.customMarginSection}>
                        <Grid container spacing={0} sx={{marginTop: 5}}>
                            {srcImages.map((image, index) => (
                                <Grid item xs={12} sm={12} md={12} lg={6} key={index}>
                                    <Box sx={{
                                        display: 'grid',
                                        gridTemplateColumns: '1fr',
                                        gridTemplateRows: 'auto auto',
                                        gap: '10px',
                                        position: 'relative'
                                    }}>
                                        <img className={styles.layananImage} src={image.src} width='100%'
                                             height={300} alt={'image-layanan-kami'}/>

                                        <Typography
                                            className={styles.layananText}
                                            sx={{fontSize: 46, fontWeight: 600}}>
                                            {image.text}
                                        </Typography>

                                        <Button
                                            onClick={() => {
                                                router.push(`/layanan/${image.detail}`)
                                            }}
                                            variant="contained"
                                            sx={{
                                                position: 'absolute',
                                                bottom: '60%',
                                                left: '50%',
                                                transform: 'translateX(-50%)',
                                                backgroundColor: 'transparent',
                                                border: '2px solid white',
                                                color: 'white',
                                                '&:hover': {
                                                    backgroundColor: 'white',
                                                    color: 'black'
                                                }
                                            }}
                                        >
                                            Lihat Selengkapnya
                                        </Button>
                                    </Box>
                                </Grid>
                            ))}
                        </Grid>
                        <Grid container spacing={0} sx={{marginY: 5}}>
                            <Grid item xs={12} sm={12} md={6}>
                                <Typography variant={'h4'} sx={{fontWeight: 600}}>
                                    {detail == 'design-interior' ?
                                        <>
                                            Desain Interior
                                        </>
                                        : detail == 'arsitektur' ?
                                            <>
                                                Arsitektur
                                            </>
                                            : undefined}
                                </Typography>
                                <Typography sx={{marginY: 5}}>
                                    {detail == 'design-interior' ?
                                        <>
                                            DCOR. menyediakan layanan desain interior berpengalaman selama 12
                                            tahun yang menyediakan solusi desain interior kreatif dan inovatif untuk
                                            rumah, apartemen, cafe, restoran, hotel, dan toko kantor.
                                        </>
                                        : detail == 'arsitektur' ?
                                            <>
                                                DCOR. adalah perusahaan arsitektur berpengalaman selama 12
                                                tahun. Kami menyediakan solusi desain arsitektur yang berkualitas dan
                                                memuaskan untuk klien kami, dari perumahan hingga bangunan komersial dan
                                                publik.
                                            </>
                                            : undefined}
                                </Typography>
                                <Button variant={'contained'} sx={{backgroundColor: "#25D366"}} size={'large'}
                                        color={'primary'} startIcon={<WhatsAppIcon/>}>Konsultasi Sekarang</Button>
                            </Grid>
                        </Grid>
                        <Box sx={{width: '80%', height: '100%', overflowY: 'scroll', margin: 'auto'}}>
                            <ImageList variant="masonry" cols={3} gap={8}>
                                {itemData.map((item) => (
                                    <ImageListItem key={item.img} sx={{cursor: 'pointer'}} className={styles.imageHover}>
                                        <img
                                            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                                            src={`${item.img}?w=248&fit=crop&auto=format`}
                                            alt={item.title}
                                            loading="lazy"
                                            onClick={() => {
                                                setOpenModal(true)
                                                setImage(item.img)
                                            }}
                                        />
                                        <ImageListItemBar position="below" sx={{fontWeight: 600}} title={item.type}/>
                                    </ImageListItem>
                                ))}
                            </ImageList>
                        </Box>
                        <ModalFile openModal={openModal} setOpenModal={setOpenModal} src={image} />
                    </Box>
                </section>
                <section id={'bisnis-proses'}>
                    <Box className={styles.customMarginSection}>
                        <Divider textAlign="left">
                            <Typography
                                variant={'h4'}
                                sx={{
                                    fontWeight: 600
                                }}>
                                BISNIS PROSES
                            </Typography>
                        </Divider>
                        <Stack direction={{xs: 'column', md: 'row'}} spacing={{xs: 1, sm: 2, md: 4}} sx={{marginY: 5}}
                               justifyContent="space-between" alignItems="center">
                            {srcImagesDcor.map((image, index) => (
                                <Box
                                    key={index}
                                    className={styles.imageDecor}
                                    sx={{
                                        backgroundImage: `url(${image.src})`,
                                        backgroundPosition: 'center center',
                                        backgroundSize: 'cover',
                                        position: 'relative'
                                    }}
                                >
                                    <Typography variant={'h5'} sx={{
                                        fontWeight: 600,
                                        position: 'absolute',
                                        bottom: 0,
                                        right: 0,
                                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                                        padding: '5px',
                                    }}>
                                        {image.text}
                                    </Typography>
                                </Box>
                            ))}
                        </Stack>
                    </Box>
                </section>
            </main>
        </>
    );
}

export default DetailPage