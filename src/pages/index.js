// TUGAS UAS
// NAMA : ADRIAN SATRIA PUTRA
// NIM  : 10123902
// KELAS: IF12

import Head from "next/head";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {TypeAnimation} from "react-type-animation";
import Button from "@mui/material/Button";
import styles from '@/styles/home.module.css';
import {Avatar, Divider, Grid, Link, Stack} from "@mui/material";
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import process from "next/dist/build/webpack/loaders/resolve-url-loader/lib/postcss";
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import {useRouter} from "next/router";
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {useState} from "react";
import ModalFile from "@/components/ModalFile";

const Home = () => {
    const [openModal, setOpenModal] = useState(false)
    const [image, setImage] = useState('')

    const router = useRouter()

    const srcImages = [
        {
            src: 'https://cdn.pixabay.com/photo/2016/12/30/07/59/kitchen-1940174_1280.jpg',
            text: 'Desain Interior',
            detail: 'design-interior'
        },
        {
            src: 'https://cdn.pixabay.com/photo/2016/11/18/17/46/house-1836070_1280.jpg',
            text: 'Arsitektur',
            detail: 'arsitektur'
        },
    ];

    const srcImagesDcor = [
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-one.svg`,
            text: '10+ Tahun Pengalaman'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-two.svg`,
            text: 'Aman & Terpercaya'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-three.svg`,
            text: 'Kualitas Terbaik'
        },
        {
            src: `${process.env.NEXT_PUBLIC_BASE_URL}/number-four.svg`,
            text: 'Profesional Tim'
        },
    ];

    const itemData = [
        {
            img: 'https://cdn.pixabay.com/photo/2021/11/28/11/54/bed-6830011_1280.jpg',
            title: 'Bed',
        },
        {
            img: 'https://images.unsplash.com/photo-1525097487452-6278ff080c31',
            title: 'Books',
        },
        {
            img: 'https://images.unsplash.com/photo-1523413651479-597eb2da0ad6',
            title: 'Sink',
        },
        {
            img: 'https://images.unsplash.com/photo-1563298723-dcfebaa392e3',
            title: 'Kitchen',
        },
        {
            img: 'https://images.unsplash.com/photo-1588436706487-9d55d73a39e3',
            title: 'Blinds',
        },
        {
            img: 'https://images.unsplash.com/photo-1574180045827-681f8a1a9622',
            title: 'Chairs',
        },
        {
            img: 'https://cdn.pixabay.com/photo/2016/06/05/22/13/home-1438305_1280.jpg',
            title: 'Living Room',
        },
        {
            img: 'https://images.unsplash.com/photo-1481277542470-605612bd2d61',
            title: 'Doors',
        },
        {
            img: 'https://cdn.pixabay.com/photo/2015/12/05/23/16/office-1078869_1280.jpg',
            title: 'Office',
        },
        {
            img: 'https://images.unsplash.com/photo-1516455207990-7a41ce80f7ee',
            title: 'Storage',
        },
        {
            img: 'https://cdn.pixabay.com/photo/2018/10/28/12/37/bedroom-3778695_1280.jpg',
            title: 'Bed Room',
        },
        {
            img: 'https://images.unsplash.com/photo-1519710164239-da123dc03ef4',
            title: 'Coffee table',
        },
    ];

    return (
        <>
            <Head>
                <title>DCOR. | Dekorasi & Arsitektur Rumah</title>
                <meta name="description"
                      content="Melayani pemesanan pembuatan furniture dan sewa jawa dekorasi rumah anda. Jadikan rumah anda menjadi impian bagi semua orang."/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" href={`${process.env.NEXT_PUBLIC_BASE_URL}/dcor-rel-logo.svg`}/>
                <meta name='keywords' content='DCOR, Dekorasi rumah, dekorasi, Rumah dekorasi'/>
            </Head>
            <main>
                <section>
                    <Box
                        sx={{
                            height: '90vh',
                            backgroundPosition: 'center',
                            backgroundSize: 'cover',
                            backgroundRepeat: 'none',
                        }}>
                        <img
                            className={styles.imageHeader}
                            src="https://cdn.pixabay.com/photo/2017/09/09/18/25/living-room-2732939_1280.jpg"
                            alt="Background"
                        />

                        <TypeAnimation
                            className={styles.animationText}
                            sequence={[
                                `SELAMAT DATANG DI DCOR.`,
                                1000,
                                '',
                                `WUJUDKAN RUMAH IMPIANMU BERSAMA KAMI`,
                                10000,
                                '',
                            ]}
                            repeat={Infinity}
                        />

                        <Button
                            onClick={() => {
                                router.push('/#mulai')
                            }}
                            sx={{
                                backgroundColor: 'white',
                                color: 'black',
                                position: 'absolute',
                                top: '50%',
                                left: '50%',
                                transform: 'translate(-50%, -50%)',
                                marginTop: '15vh',
                                '@media (max-width: 720px)': {
                                    marginTop: '35vh'
                                },
                                '&:hover': {
                                    backgroundColor: 'transparent',
                                    border: '2px solid white',
                                    color: 'white'
                                }
                            }}
                            variant="contained"
                            size={'large'}>Mulai</Button>
                    </Box>
                </section>
                <section id={'layanan-kami'}>
                    <Box className={styles.customMarginSection}>
                        <Divider textAlign="left">
                            <Typography
                                variant={'h4'}
                                sx={{
                                    fontWeight: 600
                                }}>
                                LAYANAN KAMI
                            </Typography>
                        </Divider>

                        <Grid container spacing={0} sx={{marginTop: 5}}>
                            {srcImages.map((image, index) => (
                                <Grid item xs={12} sm={12} md={12} lg={6} key={index}>
                                    <Box sx={{
                                        display: 'grid',
                                        gridTemplateColumns: '1fr',
                                        gridTemplateRows: 'auto auto',
                                        gap: '10px',
                                        position: 'relative'
                                    }}>
                                        <img className={styles.layananImage} src={image.src} width='100%'
                                             height={300} alt={'image-layanan-kami'} />

                                        <Typography
                                            className={styles.layananText}
                                            sx={{fontSize: 46, fontWeight: 600}}>
                                            {image.text}
                                        </Typography>

                                        <Button
                                            onClick={()=> {
                                                router.push(`/layanan/${image.detail}`)
                                            }}
                                            variant="contained"
                                            sx={{
                                                position: 'absolute',
                                                bottom: '60%',
                                                left: '50%',
                                                transform: 'translateX(-50%)',
                                                backgroundColor: 'transparent',
                                                border: '2px solid white',
                                                color: 'white',
                                                '&:hover': {
                                                    backgroundColor: 'white',
                                                    color: 'black'
                                                }
                                            }}
                                        >
                                            Lihat Selengkapnya
                                        </Button>
                                    </Box>
                                </Grid>
                            ))}
                        </Grid>

                        <Divider textAlign="left">
                            <Typography
                                variant={'h4'}
                                sx={{
                                    fontWeight: 600
                                }}>
                                MENGAPA DCOR?
                            </Typography>
                        </Divider>

                        <Stack direction={{xs: 'column', md: 'row'}} spacing={{xs: 1, sm: 2, md: 4}} sx={{marginY: 5}}
                               justifyContent="space-between" alignItems="center">
                            {srcImagesDcor.map((image, index) => (
                                <Box
                                    key={index}
                                    className={styles.imageDecor}
                                    sx={{
                                        backgroundImage: `url(${image.src})`,
                                        backgroundPosition: 'center center',
                                        backgroundSize: 'cover',
                                        position: 'relative'
                                    }}
                                >
                                    <Typography variant={'h5'} sx={{
                                        fontWeight: 600,
                                        position: 'absolute',
                                        bottom: 0,
                                        right: 0,
                                        backgroundColor: 'rgba(255, 255, 255, 0.2)',
                                        padding: '5px',
                                    }}>
                                        {image.text}
                                    </Typography>
                                </Box>
                            ))}
                        </Stack>
                    </Box>
                </section>
                <section id={'portofolio'}>
                    <Box className={styles.customMarginSection}>
                        <Divider textAlign="left">
                            <Typography
                                variant={'h4'}
                                sx={{
                                    fontWeight: 600,
                                    marginY: 5
                                }}>
                                PORTOFOLIO DCOR.
                            </Typography>
                        </Divider>
                        <Box sx={{width: '100%', height: 700, overflowY: 'scroll'}}>
                            <ImageList variant="masonry" cols={3} gap={8}>
                                {itemData.map((item, index) => (
                                    <ImageListItem key={index} sx={{cursor: 'pointer'}} className={styles.imageHover}>
                                        <img
                                            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                                            src={`${item.img}?w=248&fit=crop&auto=format`}
                                            alt={item.title}
                                            loading="lazy"
                                            onClick={() => {
                                                setOpenModal(true)
                                                setImage(item.img)
                                            }}
                                        />
                                    </ImageListItem>
                                ))}
                            </ImageList>
                        </Box>
                        <ModalFile openModal={openModal} setOpenModal={setOpenModal} src={image} />
                    </Box>
                </section>
                <section id={'tentang-kami'}>
                    <Box className={styles.customMarginSection}>
                        <Grid container spacing={4} padding={{xs: 2, md: 10}}>
                            <Grid item xs={12} sm={12} md={6}>
                                <Typography variant={'h4'} sx={{fontWeight: 600}}>
                                    TENTANG DCOR.
                                </Typography>
                                <Typography variant={'h6'}>
                                    Desain Interior & Arsitektur
                                </Typography>
                                <Typography sx={{marginTop: 5}}>
                                    DCOR adalah tim berpengalaman yang telah banyak dipercaya oleh client. Serta menyediakan berbagai pelayanan mulai dari desain interior
                                    hingga desain arsitektur untuk rumah, cafe, restoren, apartemen, villa, dll. Tidak hanya itu, DCOR juga menyediakan berbagai produk
                                    furniture berkualitas. Layanan professional kami didedikasikan untuk memberikan hasil berkualitas yang memenuhi seluruh kebutuhan klien kami.
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6}>
                                <Typography variant={'h4'} sx={{fontWeight: 600}}>
                                    VISI
                                </Typography>
                                <Typography variant={'h6'}>
                                    Menjadi perusahaan yang memberikan layanan jasa dan produk yang profesional, mampu menciptakan produk yang kreatif & inovatif guna membantu memenuhi kepuasan klien.
                                </Typography>
                                <Typography variant={'h4'} sx={{fontWeight: 600, marginTop: 5}} >
                                    MISI
                                </Typography>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1-content"
                                        id="panel1-header"
                                        sx={{fontWeight: 500}}
                                    >
                                        SDM Profesional
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        Mempersiapkan SDM dibidang keahliannya.
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel2-content"
                                        id="panel2-header"
                                        sx={{fontWeight: 500}}
                                    >
                                        Berkualitas
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        Menyediakan produk dan jasa yang berkualitas dalam memenuhi kebutuhan.
                                    </AccordionDetails>
                                </Accordion>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel3-content"
                                        id="panel3-header"
                                        sx={{fontWeight: 500}}
                                    >
                                        Transparan
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        Transparan dalam bekerja agar mendapatkan kepercayaan penuh dari klien.
                                    </AccordionDetails>
                                </Accordion>
                            </Grid>
                        </Grid>
                    </Box>
                </section>
                <section id={'mulai'}>
                    <Box sx={{
                        paddingY: 2,
                        backgroundColor: "rgba(227,224,224,0.51)"
                    }}>
                        <Grid container spacing={0} className={styles.customMarginSection}>
                            <Grid item xs={12} sm={6}>
                                <Typography
                                    variant={'h4'}
                                    sx={{
                                        fontWeight: 600
                                    }}>
                                    Siap bangun rumah impianmu bersama kami?
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6} sx={{textAlign: 'center', padding: 2}}>
                                <Button variant={'contained'} sx={{backgroundColor: "#25D366"}} size={'large'}
                                        color={'primary'} startIcon={<WhatsAppIcon/>}>Konsultasi Sekarang</Button>
                            </Grid>
                        </Grid>
                    </Box>
                </section>
            </main>
        </>
    );
}

export default Home
