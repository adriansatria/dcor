// TUGAS UAS
// NAMA : ADRIAN SATRIA PUTRA
// NIM  : 10123902
// KELAS: IF12

import "@/styles/globals.css";
import ResponsiveAppBar from "@/components/Appbar";
import ResponsiveFooter from "@/components/Footer";

const App = ({Component, pageProps}) => {
    return (
        <>
            <header>
                <ResponsiveAppBar/>
            </header>
            <Component {...pageProps} />
            <footer id={'contact'}>
                <ResponsiveFooter/>
            </footer>
        </>
    );
}

export default App
