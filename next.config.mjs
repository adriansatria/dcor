/** @type {import('next').NextConfig} */
const nextConfig = {
  assetPrefix: process.env.NODE_ENV != 'production' ? '' : process.env.NEXT_PUBLIC_BASE_URL,
  basePath: process.env.NODE_ENV != 'production' ? '' : process.env.NEXT_BASE_PATH,
  reactStrictMode: true,
  trailingSlash: true,
  images: {
    unoptimized: true
  },
};

export default nextConfig;
